package cat.itb.mateuyabar.m06.uf1.xml

import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XML
import java.nio.file.Path
import java.util.Scanner
import kotlin.io.path.exists
import kotlin.io.path.readText
import kotlin.io.path.writeText

@Serializable
data class SportLog(
    val sport: String,
    val time: Int
)

val sportLogFile = Path.of("sportlog.xml")

fun main() {
    val scanner = Scanner(System.`in`)
    val sports :MutableList<SportLog> = readSportLog()
    addSport(scanner, sports)
    saveSportLog(sports)
    printResume(sports)
}

fun printResume(sports: List<SportLog>) {
    sports.groupBy { it.sport }
        .mapValues { it.value.sumOf { it.time } }
        .forEach{
            println("${it.key} : ${it.value}")
        }
}

fun saveSportLog(sports: MutableList<SportLog>) {
    val xml = XML.encodeToString(sports)
    sportLogFile.writeText(xml)
}

fun addSport(scanner: Scanner, sports: MutableList<SportLog>) {
    val sport = scanner.nextLine()
    val time = scanner.nextLine().toInt()
    val sportLog = SportLog(sport, time)
    sports+=sportLog
}

fun readSportLog(): MutableList<SportLog> =
    if(sportLogFile.exists())
        XML.decodeFromString(sportLogFile.readText())
    else
        mutableListOf()

