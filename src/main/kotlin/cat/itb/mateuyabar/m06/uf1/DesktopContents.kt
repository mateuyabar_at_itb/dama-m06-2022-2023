package cat.itb.mateuyabar.m06.uf1

import java.nio.file.Path
import kotlin.io.path.createFile
import kotlin.io.path.listDirectoryEntries
import kotlin.io.path.notExists
import kotlin.io.path.writeText

fun main() {
    val home = System.getProperty("user.home")
    val desktop = Path.of(home, "Escriptori")
    val fileCount = desktop.listDirectoryEntries().size
    val resultFile = Path.of(home, "desktopContents.txt")
    resultFile.writeText(fileCount.toString())
}