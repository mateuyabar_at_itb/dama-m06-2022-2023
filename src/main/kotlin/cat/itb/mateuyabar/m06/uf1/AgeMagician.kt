package cat.itb.mateuyabar.m06.uf1

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import java.util.Scanner

suspend fun main() {
    val scanner = Scanner(System.`in`)
    println("Ben vingut al AgeMagician. Com et dius?")
    val name = scanner.nextLine()
    val age = calculateAge(name)
    println("Jo crec que tens $age anys!")
}

@Serializable
data class AgifyData(val age: Int)

suspend fun calculateAge(name: String):Int{
    val url = "https://api.agify.io/?name=$name&country_id=ES"
    val client = HttpClient(CIO){
        install(ContentNegotiation) {
            json(Json {
                ignoreUnknownKeys = true
            })
        }
    }
    val agifyData : AgifyData = client.get(url).body()
    return agifyData.age
}