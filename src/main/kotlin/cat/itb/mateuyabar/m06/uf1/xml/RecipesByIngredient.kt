package cat.itb.mateuyabar.m06.uf1.xml

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XML
import nl.adaptivity.xmlutil.serialization.XmlChildrenName
import nl.adaptivity.xmlutil.serialization.XmlElement
import java.util.Scanner

@Serializable
@SerialName("recipes")
data class Recipes(val recipes: List<Recipe>)

@Serializable
@SerialName("recipe")
data class Recipe(
    val dificulty: Int,
    @XmlElement(true)
    val name: String,
    @XmlChildrenName("ingredient", "", "")
    @SerialName("ingredients")
    val ingredients: List<Ingredient>
)

@Serializable
data class Ingredient(
    val ammount: Int,
    val unit: String,
    val name: String
)

fun main() {
//    printSampleRecipe()
    val scanner = Scanner(System.`in`)
    val ingredient = askIngredient(scanner)
    val recipes = readRecipes()
    val recipesToPrint: List<Recipe> = filterByIngredient(recipes, ingredient)
    printRecipes(recipesToPrint)
}

fun printRecipes(recipesToPrint: List<Recipe>) {
    recipesToPrint.forEach{
        println(it) // TODO pretty print
    }
}

fun filterByIngredient(recipes: Recipes, ingredient: String) =
    recipes.recipes.filter { recipe ->
        recipe.ingredients.any{it.name == ingredient}
    }


fun askIngredient(scanner: Scanner): String {
    println("Ingredient?")
    return scanner.nextLine()
}

fun readRecipes(): Recipes {
    val xml = Recipes::class.java.getResource("/receptes.xml").readText()
    return  XML.Companion.decodeFromString<Recipes>(xml)
}
