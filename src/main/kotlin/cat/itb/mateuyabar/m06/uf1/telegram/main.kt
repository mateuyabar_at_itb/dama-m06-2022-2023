package cat.itb.mateuyabar.m06.uf1.telegram

import cat.itb.mateuyabar.m06.uf1.xml.filterByIngredient
import cat.itb.mateuyabar.m06.uf1.xml.readRecipes
import com.github.kotlintelegrambot.bot
import com.github.kotlintelegrambot.dispatch
import com.github.kotlintelegrambot.dispatcher.command
import com.github.kotlintelegrambot.dispatcher.text
import com.github.kotlintelegrambot.entities.ChatId

val chatIds : MutableList<Long> = mutableListOf()
fun main() {
    val bot = bot {
        token = "TODO"
        dispatch {
            command("listen"){
                chatIds += message.chat.id
            }
            text{
            }
            command("sayHello"){
                chatIds.forEach{
                    bot.sendMessage(ChatId.fromId(it), text = "Hello")
                }
            }
            text("hola") {
                bot.sendMessage(ChatId.fromId(message.chat.id), text = "Adéu")
            }

            command("recipes"){
                val ingredient = args[0]
                val recipes = readRecipes()
                val filtered = filterByIngredient(recipes, ingredient)
                bot.sendMessage(ChatId.fromId(message.chat.id), text = filtered.toString())
            }
        }
    }
    bot.startPolling()
}