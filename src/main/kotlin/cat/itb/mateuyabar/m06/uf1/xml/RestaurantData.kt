package cat.itb.mateuyabar.m06.uf1.xml

import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import nl.adaptivity.xmlutil.serialization.XML
import nl.adaptivity.xmlutil.serialization.XmlElement

@Serializable
@SerialName("restaurant")
data class Restaurant(@XmlElement(true) val name: String,
                      val type: String,
                      @XmlElement(true) val address: String,
                      @XmlElement(true) val owner: String)

fun main() {
    val xml = Restaurant.javaClass.getResource("/restaurant.xml").readText()
    val restaurant : Restaurant =XML.decodeFromString(xml)
    println(restaurant)
    // TODO millorar impresió
}