package cat.itb.mateuyabar.m06.uf2

import org.jetbrains.exposed.dao.id.IntIdTable
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction

object Persons: IntIdTable(){
    val name = varchar("name", 100)
    val surname = varchar("surname", 100)
}

data class Person(val id: Int, val name: String, val surname: String)

class PersonDao{
    fun insert(name: String, surname: String) = transaction{
        Persons.insert {
            it[Persons.name] = name
            it[Persons.surname] = surname
        }
    }

    fun list(): List<Person> = transaction {
        Persons.selectAll().map(::mapper)
    }

    fun find(id: Int) : Person? = transaction {
        Persons.select { Persons.id eq id }
            .map(::mapper)
            .singleOrNull()
    }

    fun mapper(resultRow : ResultRow) =
        Person(resultRow[Persons.id].value, resultRow[Persons.name], resultRow[Persons.surname])

}

fun main() {
    val personDao = PersonDao()
    Database.connect("jdbc:h2:./sportdb.h2", "org.h2.Driver")
    transaction {
        addLogger(StdOutSqlLogger)
        SchemaUtils.create(SportLog)
    }

    personDao.insert("Joan", "Puig")
    val list : List<Person> = personDao.list()
}