package cat.itb.mateuyabar.m06.uf2

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.time.LocalDateTime

object DateLog : Table(){
    val date = varchar("date", 50)
}

fun main() {
    Database.connect("jdbc:h2:./mydb.h2", "org.h2.Driver")
    transaction {
        addLogger(StdOutSqlLogger)
        SchemaUtils.create(DateLog)
    }
    transaction {
        DateLog.insert {
            it[DateLog.date] = LocalDateTime.now().toString()
        }
    }
    transaction {
        DateLog.selectAll().forEach {
            println(it[DateLog.date])
        }
    }
}