package cat.itb.mateuyabar.m06.uf2

fun main() {
    val list = listOf(1,4,56,4,32,2)
    val isTwo : (Int) -> Boolean = {
        it==2
    }
    list.filter{
        true
    }

    val list2 = listOf("ads","dsadsa","dsasdadsa","asdfsd")
    list2.sortedBy(String::length)
}

fun isTwo(value: Int) : Boolean {
    return value==2
}