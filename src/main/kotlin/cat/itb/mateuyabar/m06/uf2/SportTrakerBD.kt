package cat.itb.mateuyabar.m06.uf2

import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.transaction
import java.util.Scanner

object SportLog : Table(){
    val name = varchar("name", 100)
    val time = integer("time")
}

fun main() {
    Database.connect("jdbc:h2:./sportdb.h2", "org.h2.Driver")
    transaction {
        addLogger(StdOutSqlLogger)
        SchemaUtils.create(SportLog)
    }
    val sport = readLine()!!
    val time = readLine()!!.toInt()
    transaction {
        SportLog.insert {
            it[SportLog.name] = sport
            it[SportLog.time] = time
        }
    }

    transaction {
        val timeSum = SportLog.time.sum()
        // Select name, sum(time) FROM SportLog Group BY name
        SportLog.slice(SportLog.name, timeSum)
            .selectAll().groupBy(SportLog.name)
            .forEach {
                println("${it[SportLog.name]}:  ${it[timeSum]}")
            }
    }
}