//package cat.itb.mateuyabar.m06.uf2
//
//import org.jetbrains.exposed.sql.*
//import org.jetbrains.exposed.sql.transactions.transaction
//
//object Persons : Table(){
//    val id: Column<Int> = integer("id").autoIncrement()
//    val name = varchar("name", 250)
//    val surname = varchar("surname", 250)
//}
//
//
//
//fun main() {
//    Database.connect("jdbc:h2:mem:regular", "org.h2.Driver")
//
//    transaction {
//        addLogger(StdOutSqlLogger)
//        SchemaUtils.create (Persons)
//
//        Persons.insert {
//            it[name] = "Pere"
//            it[surname] = "Pasqual"
//        }
//
//        Persons.insert {
//            it[name] = "Mar"
//            it[surname] = "Pasqual"
//        }
//
//
//        Persons.select { Persons.name eq "Mar" }.forEach {
//            println(it[Persons.name]+" "+it[Persons.surname])
//        }
//
//    }
//
//}